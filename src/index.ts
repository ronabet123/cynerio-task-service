import * as mongoose from 'mongoose';
import { Server } from './server';
import { config } from './config';

process.on('uncaughtException', (err) => {
    console.error('Unhandled Exception', err.stack);
    process.exit(1);
});

process.on('unhandledRejection', (err) => {
    console.error('Unhandled Rejection', err);
    process.exit(1);
});

process.on('SIGINT', async () => {
    try {
        console.log('User Termination');
        await mongoose.disconnect();
        process.exit(0);
    } catch (error) {
        console.error('Faild to close connections', error);
    }
});
(async () => {
    await mongoose.connect(config.db.connectionString, {
        useUnifiedTopology: true, 
        useNewUrlParser: true, 
    });

    console.log(`[MongoDB] connected to port 27017`);
    console.log('Starting server');
    const server: Server = Server.bootstrap();

    server.app.on('close', () => {
        mongoose.disconnect();
        console.log('Server closed');
    });
})();
