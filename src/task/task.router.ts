import { Router } from 'express';
import { Wrapper } from '../utils/wrapper';
import { TaskController } from './task.controller';

const TaskRouter: Router = Router();

TaskRouter.post('/checkin', Wrapper.wrapAsync(TaskController.checkIn));
TaskRouter.put('/checkout/:user', Wrapper.wrapAsync(TaskController.checkOut));
TaskRouter.get('/report', Wrapper.wrapAsync(TaskController.getReport));


export { TaskRouter };
