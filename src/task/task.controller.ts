import { Request, Response } from 'express';
import { TaskManager } from './task.manager';
import {IUser, taskObj} from './task.interface';
import { format } from 'date-fns'
import {
    StatusCodes,
} from 'http-status-codes';

export class TaskController {
    
    static checkOut = async (req: Request, res: Response) => {
        res.json(await TaskManager.checkOut(req.params.user as String, format(new Date(), "yyyy-MM-dd'T'HH:mm:ss")));
    }

    static checkIn = async (req: Request, res: Response) => {
        const userState = await TaskManager.getUserState(req.body.user)
        const taskObj : taskObj = {task: req.body.task, startTime: format(new Date(), "yyyy-MM-dd'T'HH:mm:ss"), endTime: "", time: ""}
        if (userState && userState.checkoutStatus === false) {
            res.json({error: `User '${req.body.user}' is already on task, please check-out before a new check-in!`}).status(StatusCodes.BAD_REQUEST)
        } else {
            const newUser : IUser = {
                user: req.body.user, checkoutStatus: false, tasks: [taskObj]
            }
            res.json(await TaskManager.userCheckIn(newUser as IUser, taskObj as taskObj));
        }
    }

    static getReport = async (req: Request, res: Response) => {
        res.send(await TaskManager.getReport());
    }

}


